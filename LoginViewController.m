//
//  LoginViewController.m
//  ios-ogre
//
//  Created by Barnabás Birmacher on 29/11/13.
//
//

#import "LoginViewController.h"
#import "ViewController.h"
#import "AppDelegate.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginPressed:(id)sender {
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    
//    ViewController* vc = [[ViewController alloc] initWithNibName:@"ViewController"
//                                                          bundle:nil];
//    [vc startWithWindow:appDelegate.mWindow];
    
    [self.navigationController pushViewController:appDelegate.mViewController
                                         animated:YES];
}
@end
